#include <stdlib.h>
#include <string.h>

#include "array.h"

#include <stdio.h>

struct dyn_array init_array(__u8 num_elem, __u8 elem_size){

	struct dyn_array da = {
		.array = (void **)malloc(sizeof(void *) * num_elem),
		.size = num_elem,
		.elem_size = elem_size,
		.count = 0
	};

	return da;
}

void add_element(struct dyn_array *array, void *elem){

	if(array->count == array->size){
		array->size = array->size >= 0x7f ? 0xff : array->size << 1;
		array->array = (void **)realloc(array->array, array->size * sizeof(void *));
	}

	__u8 index = array->count++;
	array->array[index] = malloc(array->elem_size);
	//check return
	memcpy(array->array[index], elem, array->elem_size);
}

__s16 find_element(struct dyn_array *array, void *obj, __u8 (*func)(void *, void*)){

	__u8 index;
	for(index = 0; index < array->count && !func(array->array[index], obj); index++);

	return index < array->count ? index : -1;
}

void remove_by_index(struct dyn_array *array, __u8 index){

	free(array->array[index]);

	if(index < (array->count - 1)){
		memcpy(array->array + index, array->array + index + 1, sizeof(void *) * (array->count - index - 1));
	}

	array->count--;
}

__s16 remove_by_element(struct dyn_array *array, void *obj, __u8 (*func)(void *, void*)){

	__s8 index = find_element(array, obj, func);

	if(index >= 0){
		remove_by_index(array, index);
	}

	return index;
}

__s16 any_element(struct dyn_array *array, __u8 (*func)(void *)){

	__u8 index;
	for(index = 0; index < array->count && !func(array->array[index]); index++);

	return index < array->count ? index : -1;
}

void free_array(struct dyn_array *array){

	for(__u8 index = 0; index < array->count; index++){
		free(array->array[index]);
	}
	
	free(array->array);
}
