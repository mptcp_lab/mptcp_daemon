#ifndef ___DAEMON_MPTPC_NETWORK_H___
#define ___DAEMON_MPTPC_NETWORK_H___

#include <asm/types.h>
#include "array.h"

struct network_info{
	__u8 family;
	__u8 net[16];
	__u8 bytelen;
	__u8 bitlen;
};

struct mptcp_endpoint{
	struct network_info info;
	__u32 mptcp_id;
	__u32 table_id;
	__u8 configured;
};

struct network_list{
	struct dyn_array ipv4;
	struct dyn_array ipv6;
};

__u8 check_network(struct dyn_array *list, struct network_info *network);

__u8 compare_net_info(void *elem, void *obj);

__u8 is_link_local(__u8 addr[16]);

__s8 get_address_mask(char *mask, __u8 max, __u8 *out);

__s8 add_network(struct network_list *array, char *network);

__u8 is_ipv6(void *obj);

__u8 is_ipv4(void *obj);

__s16 parse_proto(char *proto);

void print_network(struct network_info *net, char* add_str);

#endif
