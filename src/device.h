#ifndef ___DAEMON_MPTCP_DEVICE_H___
#define ___DAEMON_MPTCP_DEVICE_H___

#include "array.h"
#include "network.h"

struct dev_info{
	__u16 index;
	struct dyn_array addr;
	struct dyn_array dest;
	struct network_info *gw[2];
	__u8 addrs_conf;
};

struct dev_hash{
	struct dev_info **tb;
	__u16 size;
};

struct dev_reply{
	__u16 index;
	__u8 data;
};

struct dev_hash init_dev_hash(__u16 size);

void add_key(struct dev_hash *dh, __u16 key);

void remove_key(struct dev_hash *dh, __u16 key);

int add_dev(struct nlmsghdr *n, void *arg);

int rm_dev(struct nlmsghdr *n, struct dev_hash *interfaces);

int add_addr(struct nlmsghdr *n, struct dev_hash *arg, __u16 *out);

int add_addr_wrapper(struct nlmsghdr *n, void *arg);

int rm_addr(struct nlmsghdr *n, struct dev_hash *interfaces, struct dev_reply *rep);

int add_route(struct nlmsghdr *n, struct dev_hash *interfaces, struct dev_reply *rep);

int add_route_wrapper(struct nlmsghdr *n, void *arg);

int rm_route(struct nlmsghdr *n, struct dev_hash *interfaces, struct dev_reply *rep);

#endif
