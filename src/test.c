#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <linux/fib_rules.h>

#include <asm/types.h>
#include <libnetlink.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/if_addr.h>
#include <linux/if_link.h>
#include <linux/genetlink.h>
#include <linux/mptcp.h>

#include <sys/socket.h>
#include <unistd.h>

#include "req.h"

#define BUFFSIZE 8182

int main(int argc, char **argv){
	struct rtnl_handle rth = { .fd = -1 }; //para todas as comunicações excepto mptcp

    //para toda a comunicação excepto mptcp
    if(rtnl_open(&rth, 0) < 0){
        fprintf(stderr,"Failed to open rtnetlink connection!");
        _exit(-1);
    }

	//BUFFSIZE bytes
	struct nlmsghdr *req = (struct nlmsghdr *)calloc(BUFFSIZE, sizeof(char));
	size_t size = BUFFSIZE;

	size_t total_size = 0;

	struct dev_info dev = {
		.index = 3,
		.addr = {192,168,1,73},
		.bitlen = 24,
		.bytelen = 4,
		.dest = {{192,168,1,0}},
		.family = AF_INET,
		.gw = {192,168,1,254},
		.table_id = 1,
		.mptcp_id = 1
	};

	fprintf(stdout, "Size Zero Part: %u\n", req->nlmsg_len);

	add_rule(req, &dev, NLM_F_MULTI);

	total_size += req->nlmsg_len + sizeof(struct nlmsghdr);

	fprintf(stdout, "Size First Part: %u\n", req->nlmsg_len);

	struct nlmsghdr *next_part = (struct nlmsghdr *)(((char *) req) +
									NLMSG_ALIGN(req->nlmsg_len));

	add_route_dest(next_part, &dev, 0, NLM_F_MULTI);

	total_size += next_part->nlmsg_len + sizeof(struct nlmsghdr);

	fprintf(stdout, "Size Second Part: %u\n", next_part->nlmsg_len);

	next_part = (struct nlmsghdr *)(((char *) next_part) +
				 NLMSG_ALIGN(next_part->nlmsg_len));

	add_route_def(next_part, &dev, NLM_F_MULTI);

	total_size += next_part->nlmsg_len + sizeof(struct nlmsghdr);

	fprintf(stdout, "Size Third Part: %u\n", next_part->nlmsg_len);

	next_part = (struct nlmsghdr *)(((char *) next_part) +
				 NLMSG_ALIGN(next_part->nlmsg_len));

	next_part->nlmsg_len = 0;
	next_part->nlmsg_flags = NLM_F_REQUEST | NLM_F_MULTI;
	next_part->nlmsg_pid = 0;
	next_part->nlmsg_seq = 0;
	next_part->nlmsg_type = NLMSG_DONE;

	total_size += next_part->nlmsg_len + sizeof(struct nlmsghdr);

	fprintf(stdout, "Total size to send: %lu\n", total_size);

	struct iovec iov = {req, total_size};

	if(rtnl_talk_iov(&rth, &iov, 1, NULL) < 0){
		fprintf(stderr, "ERROR\n");
		_exit(-1);
	}

    return 0;

}
