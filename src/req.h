#ifndef ___DAEMON_MPTPC_REQ_H___
#define ___DAEMON_MPTPC_REQ_H___

#include <asm/types.h>
#include "network.h"

struct request{
	struct nlmsghdr *req;
	struct nlmsghdr *next_part;
	__u64 total_size;
	__u64 max_size;
};

#define ADD_RULE_SIZE 56
#define DEL_RULE_SIZE 36
#define ADD_ROUTE_SIZE 64
#define DEL_ROUTE_SIZE 64
#define CONF_LIMS_SIZE 36
#define CONF_SUB_SIZE 28
#define CONF_ADDR_SIZE 28
#define ADD_ENDP_SIZE 64
#define DEL_ENDP_SIZE 32
#define FLU_ENDP_SIZE 20
#define MP_END_SIZE 16

struct request init_request(__u64 size);

void update_req_part(struct request *req);

void update_req_size(struct request *req, __u64 next_part);

void finalize_multipart(struct request *req);

void add_rule(void *pointer, struct mptcp_endpoint *address, __u16 extra_flags);

void del_rule(void *pointer, struct mptcp_endpoint *address, __u16 extra_flags);

void add_destiny(void *pointer, struct network_info *dest, __u32 table_id, __u32 index, __u16 extra_flags);

void del_destiny(void *pointer, struct network_info *dest, __u32 table_id, __u32 index, __u16 extra_flags);

void add_gateway(void *pointer, struct network_info *gateway, __u32 table_id, __u32 index, __u16 extra_flags);

void del_gateway(void *pointer, struct network_info *gateway, __u32 table_id, __u32 index, __u16 extra_flags);

void conf_mptcp_limits(void *pointer, __u32 subflows, __u32 addrs, __u16 extra_flags);

void conf_mptcp_limits_subflows(void *pointer, __u32 subflows, __u16 extra_flags);

void conf_mptcp_limits_addrs(void *pointer, __u32 addrs, __u16 extra_flags);

void add_mptcp_endpoint(void *pointer, struct mptcp_endpoint *endpoint, __u32 index, __u16 extra_flags);

void del_mptcp_endpoint(void *pointer, struct mptcp_endpoint *endpoint, __u16 extra_flags);

void flush_mptcp_endpoints(void *pointer, __u16 extra_flags);

#endif
