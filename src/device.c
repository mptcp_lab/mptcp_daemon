#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libnetlink.h>
#include <linux/if.h>

#include "device.h"
#include "network.h"

//ver print_operstate ipaddress.c iproute2
#define OPERSTATE_DOWN 2
#define OPERSTATE_UP 6

__u32 mptcp_id = 1;
__u32 table_id = 1;

//find another cleaner way
extern struct network_list whitelist;
extern __u8 use_prim;
extern __u8 use_sec;
extern __u8 protocol;

struct dev_hash init_dev_hash(__u16 size){
	struct dev_hash dh = {
		.size = size,
		.tb = (struct dev_info **)calloc(size, sizeof(struct dev_info *))
	};

	return dh;
}

void add_key(struct dev_hash *dh, __u16 key){

	if(key >= dh->size){
		__u16 old_size = dh->size;
		dh->size = key >= 0x7FFF ? 0xFFFF : key << 1; //macro para 0x7fff e 0xffff
		dh->tb = (struct dev_info **)realloc(dh->tb, dh->size * sizeof(struct dev_info *));
		memset(dh->tb + old_size, 0, (dh->size - old_size) * sizeof(struct dev_info *));
	}

	dh->tb[key] = (struct dev_info *)calloc(1, sizeof(struct dev_info));
	struct dev_info *dev = dh->tb[key];

	dev->index = key;
	dev->addr = init_array(4, sizeof(struct mptcp_endpoint)); //macro para 4
	dev->dest = init_array(16, sizeof(struct network_info)); //macro pra 16
}

void remove_key(struct dev_hash *dh, __u16 key){

	struct dev_info *dev = dh->tb[key];

	free_array(&dev->addr);
	free_array(&dev->dest);

	if(dev->gw[0]){
		free(dev->gw[0]);
	}

	if(dev->gw[1]){
		free(dev->gw[1]);
	}

	free(dev);

	dh->tb[key] = NULL;
}

int add_dev(struct nlmsghdr *n, void *arg){

    struct ifinfomsg *ifi = NLMSG_DATA(n);
    int len = n->nlmsg_len;

    if(n->nlmsg_type != RTM_NEWLINK){
        return 0;
	}

    len -= NLMSG_LENGTH(sizeof(*ifi));

	if(len < 0){
		return -1;
	}

	if(!(ifi->ifi_flags & IFF_UP) || (ifi->ifi_flags & IFF_LOOPBACK)){
		return 0;
	}

	struct dev_hash *interfaces = (struct dev_hash *)arg;

	__u16 index = ifi->ifi_index;
    __u8 is_up = 0;
    for(struct rtattr *rta = IFLA_RTA(ifi); RTA_OK(rta, len); rta = RTA_NEXT(rta, len)){

        //recheck IFLA_LINK && IFLA_LINKMODE && IFLA_INFO_KIND
        if(rta->rta_type == IFLA_LINKINFO){
            return 0;
		}
        
        if(rta->rta_type == IFLA_OPERSTATE){
            if(*(__u8 *)RTA_DATA(rta) != OPERSTATE_UP){
                if(index < interfaces->size && interfaces->tb[index]){
                    return index;
                }
                return 0;
            }
            is_up = 1;
        } 
    }
        
    if(is_up && (index >= interfaces->size || !(interfaces->tb[index]))){
        add_key(interfaces, index);
	}else{
		fprintf(stdout, "device ignored\n");
		fflush(stdout);
	}


    return 0;
}

int rm_dev(struct nlmsghdr *n, struct dev_hash *interfaces){

    struct ifinfomsg *ifi = NLMSG_DATA(n);
    int len = n->nlmsg_len;

    if(n->nlmsg_type != RTM_DELLINK){
        return 0;
	}

    len -= NLMSG_LENGTH(sizeof(*ifi));

	if(len < 0){
		return -1;
	}

    //verify more flags
    if(ifi->ifi_flags & IFF_LOOPBACK){
        return 0;
	}

    __u16 index = ifi->ifi_index;
    
	if(index >= interfaces->size || !interfaces->tb[index]){
		return 0;
	}

	fprintf(stdout,"to remove device\n");
	fflush(stdout);

    return index;
}

int add_addr(struct nlmsghdr *n, struct dev_hash *interfaces, __u16 *out){

    struct ifaddrmsg *ifa = NLMSG_DATA(n);
    int len = n->nlmsg_len;

    if(n->nlmsg_type != RTM_NEWADDR){
        return 0;
	}

    len -= NLMSG_LENGTH(sizeof(*ifa));

	if(len < 0){
		return -1;
	}

	//find a better way
	if(protocol == AF_UNSPEC && ifa->ifa_family != AF_INET && ifa->ifa_family != AF_INET6){
		return 0;
	}else if(protocol == AF_INET && ifa->ifa_family != AF_INET){
		return 0;
	}else if(protocol == AF_INET6 && ifa->ifa_family != AF_INET6){
		return 0;
	}

    __u16 index = ifa->ifa_index;

    if(index >= interfaces->size || !interfaces->tb[index]){
        return 0;
	}

    __u32 flags = ifa->ifa_flags;

	struct mptcp_endpoint temp = {
		.info = {
			.bitlen = ifa->ifa_prefixlen,
			.family = ifa->ifa_family
		},
		.configured = 0
	};

	for(struct rtattr *rta = IFA_RTA(ifa); RTA_OK(rta, len); rta = RTA_NEXT(rta, len)){
		switch(rta->rta_type) {
			case IFA_FLAGS:
				flags = *(__u32 *)RTA_DATA(rta);
				break;
			case IFA_ADDRESS: //|| IFA_LOCAL
				temp.info.bytelen = RTA_PAYLOAD(rta);
				memcpy(temp.info.net, RTA_DATA(rta), temp.info.bytelen);
				break;
		}
	}

	__u8 is_sec = flags & IFA_F_SECONDARY;
    if((is_sec && use_sec) || (!is_sec && use_prim)){

		print_network(&temp.info, "");

		if(temp.info.family == AF_INET6 && is_link_local(temp.info.net)){
			fprintf(stdout, "link-local ignored\n");
			return 0;
		}

		struct dyn_array *addr = &interfaces->tb[index]->addr;

		if(find_element(addr, &temp, compare_net_info) != -1){
			fprintf(stdout, "address ignored\n");
			fflush(stdout);
			return 0;
		}

		//find a better and cleaner way
		if((temp.info.family == AF_INET && (!whitelist.ipv4.count || check_network(&whitelist.ipv4, &temp.info))) || 
			(temp.info.family == AF_INET6 && (!whitelist.ipv6.count || check_network(&whitelist.ipv6, &temp.info)))){

			temp.mptcp_id = mptcp_id++;
			temp.table_id = table_id++;

    		add_element(addr, &temp);

			if(out){
				*out = index;
			}

        	return 1;
		}else{
			fprintf(stdout, "Not in whitelist; Key removed\n");
			fflush(stdout);
			//find a better and cleaner way
			if(!interfaces->tb[index]->addrs_conf){
				remove_key(interfaces, index);
				return 0;
			}else{
				if(out){
					*out = index;
				}
				return 2;
			}
		}
    }
        
    return 0;
}

int add_addr_wrapper(struct nlmsghdr *n, void *arg){

	return add_addr(n, (struct dev_hash *)arg, NULL);

}

int rm_addr(struct nlmsghdr *n, struct dev_hash *interfaces, struct dev_reply *rep){

    struct ifaddrmsg *ifa = NLMSG_DATA(n);
    int len = n->nlmsg_len;

    if(n->nlmsg_type != RTM_DELADDR){
        return 0;
	}

    len -= NLMSG_LENGTH(sizeof(*ifa));

	if(len < 0){
		return -1;
	}

	//find a better way
	if(protocol == AF_UNSPEC && ifa->ifa_family != AF_INET && ifa->ifa_family != AF_INET6){
		return 0;
	}else if(protocol == AF_INET && ifa->ifa_family != AF_INET){
		return 0;
	}else if(protocol == AF_INET6 && ifa->ifa_family != AF_INET6){
		return 0;
	}

	__u16 index = ifa->ifa_index;

    if(index >= interfaces->size || !interfaces->tb[index]){
        return 0;
	}

    __u32 flags = ifa->ifa_flags;

	struct network_info temp = {
		.family = ifa->ifa_family,
		.bitlen = ifa->ifa_prefixlen
	};

	for(struct rtattr *rta = IFA_RTA(ifa); RTA_OK(rta, len); rta = RTA_NEXT(rta, len)){
		switch(rta->rta_type){
			case IFA_FLAGS:
				flags = *(__u32 *)RTA_DATA(rta);
				break;
			case IFA_ADDRESS: //|| IFA_LOCAL
				temp.bytelen = RTA_PAYLOAD(rta);
				memcpy(temp.net, RTA_DATA(rta), temp.bytelen);
				break;
		}
	}
    
	__u8 is_sec = flags & IFA_F_SECONDARY;
    if((is_sec && use_sec) || (!is_sec && use_prim)){
		
		if(temp.family == AF_INET6 && is_link_local(temp.net)){
			return 0;
		}

		__s16 addr_index = find_element(&interfaces->tb[index]->addr, &temp, compare_net_info);

		if(addr_index < 0){
			return 0;
		}

		rep->index = index;
		rep->data = addr_index;
		fprintf(stdout,"to remove address\n");
		fflush(stdout);

		return 1;
    }

    return 0;
}

int add_route(struct nlmsghdr *n, struct dev_hash *interfaces, struct dev_reply *rep){

    struct rtmsg *rt = NLMSG_DATA(n);
    int len = n->nlmsg_len;

    if(n->nlmsg_type != RTM_NEWROUTE){
        return 0;
	}

    len -= NLMSG_LENGTH(sizeof(*rt));

	if(len < 0){
		return -1;
	}

	//find a better way
	if(protocol == AF_UNSPEC && rt->rtm_family != AF_INET && rt->rtm_family != AF_INET6){
		return 0;
	}else if(protocol == AF_INET && rt->rtm_family != AF_INET){
		fprintf(stdout, "not ipv4; ignored\n");
		return 0;
	}else if(protocol == AF_INET6 && rt->rtm_family != AF_INET6){
		fprintf(stdout, "not ipv6; ignored\n");
		return 0;
	}

    if(rt->rtm_type != RTN_UNICAST){
        return 0;
	}

    __u32 table = rt->rtm_table;
	__u32 index = 0;
	__u8 op = 0;

	struct network_info temp = {
		.family = rt->rtm_family,
		.bitlen = rt->rtm_dst_len
	};

	for(struct rtattr *rta = RTM_RTA(rt); RTA_OK(rta, len); rta = RTA_NEXT(rta, len)){
		switch(rta->rta_type) {
			case RTA_TABLE:
				table = *(__u32 *)RTA_DATA(rta);
				break;
			case RTA_OIF:
				index = *(__u32 *)RTA_DATA(rta);
				break;
			case RTA_GATEWAY:
				op++;
			case RTA_DST:
				op++;
				temp.bytelen = RTA_PAYLOAD(rta);
				memcpy(temp.net, RTA_DATA(rta), temp.bytelen);
				break;
		}
	}

	if(index >= interfaces->size || !interfaces->tb[index]){
		return 0;
	}

	if(table != RT_TABLE_MAIN){
		return 0;
	}

	if(op && rep){
		rep->index = index;
	}

	struct dev_info *info = interfaces->tb[index];
    if(op == 2){
		__u8 gw;
		if(temp.family == AF_INET){
			gw = 0;
		}else if(!info->gw[1] || is_link_local(temp.net) || !is_link_local(info->gw[1]->net)){
			gw = 1;
		}else{
			return 0;
		}

		if(!info->gw[gw]){
			info->gw[gw] = (struct network_info *)malloc(sizeof(struct network_info));
		}

		memcpy(info->gw[gw], &temp, sizeof(struct network_info));

		if(rep){
			rep->data = gw;
		}

    }else if(op == 1){
		struct dyn_array *routes = &info->dest;

		if(temp.family == AF_INET6 && is_link_local(temp.net)){
			return 0;
		}

		if(find_element(routes, &temp, compare_net_info) != -1){
			fprintf(stdout, "route ignored\n");
			fflush(stdout);
			return 0;
		}

		add_element(routes, &temp);
	}

    return op; 
}

int add_route_wrapper(struct nlmsghdr *n, void *arg){
	return add_route(n, (struct dev_hash *)arg, NULL);
}

int rm_route(struct nlmsghdr *n, struct dev_hash *interfaces, struct dev_reply *rep){

    struct rtmsg *rt = NLMSG_DATA(n);
    int len = n->nlmsg_len;

    if(n->nlmsg_type != RTM_DELROUTE){
        return 0;
	}

    len -= NLMSG_LENGTH(sizeof(*rt));

	if(len < 0){
		return -1;
	}

	//find a better way
	if(protocol == AF_UNSPEC && rt->rtm_family != AF_INET && rt->rtm_family != AF_INET6){
		return 0;
	}else if(protocol == AF_INET && rt->rtm_family != AF_INET){
		return 0;
	}else if(protocol == AF_INET6 && rt->rtm_family != AF_INET6){
		return 0;
	}

    if(rt->rtm_type != RTN_UNICAST){
        return 0;
	}

    __u32 table = rt->rtm_table;
	__u32 index = 0;
	__u8 op = 0;

	struct network_info temp = {
		.family = rt->rtm_family,
		.bitlen = rt->rtm_dst_len
	};

	for(struct rtattr *rta = RTM_RTA(rt); RTA_OK(rta, len); rta = RTA_NEXT(rta, len)){
		switch(rta->rta_type) {
			case RTA_TABLE:
				table = *(__u32 *)RTA_DATA(rta);
				break;
			case RTA_OIF:
				index = *(__u32 *)RTA_DATA(rta);
				break;
			case RTA_GATEWAY:
				op++;
			case RTA_DST:
				op++;
				temp.bytelen = RTA_PAYLOAD(rta);
				memcpy(temp.net, RTA_DATA(rta), temp.bytelen);
				break;
		}
	}

	if(index >= interfaces->size || !interfaces->tb[index]){
		return 0;
	}

	if(table != RT_TABLE_MAIN){
		return 0;
	}

	if(op){
		rep->index = index;
	}

	struct dev_info *info = interfaces->tb[index];

    if(op == 2){
		if(temp.family == AF_INET){
			rep->data = 0;
		}else if(info->gw[1] && (is_link_local(temp.net) || !is_link_local(info->gw[1]->net))){
			rep->data = 1;
		}else{
			return 0;
		}

    }else if(op == 1){

		if(temp.family == AF_INET6 && is_link_local(temp.net)){
			return 0;
		}

		__s16 dest_index = find_element(&info->dest, &temp, compare_net_info);
		if(dest_index < 0){
			return 0;
		}

		rep->data = dest_index;
		fprintf(stdout,"to remove route\n");
		fflush(stdout);
	}

    return op;
}
