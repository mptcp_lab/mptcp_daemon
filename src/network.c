#include <stdlib.h>
#include <arpa/inet.h>

#include "network.h"

#include <string.h>

#include <stdio.h>

__u8 check_network(struct dyn_array *list, struct network_info *network){

	for(__u8 i=0; i < list->count; i++){

		struct network_info *elem = (struct network_info *)list->array[i];

		__u8 mask_bytes = elem->bitlen >> 3;
		__u8 mask_remainder = elem->bitlen & 0x7;

		if(!memcmp(network->net, elem->net, mask_bytes)){
			if(mask_remainder){

				__u16 bit_mask = ((2 << (mask_remainder - 1)) - 1) << (8 - mask_remainder);

				if((elem->net[mask_bytes] & bit_mask) == (network->net[mask_bytes] & bit_mask)){
					return 1;
				}

			}else{
				return 1;
			}
		}
	}

	return 0;
}

__u8 compare_net_info(void *elem, void *obj){

	struct network_info *elem_net = (struct network_info *)elem;
	struct network_info *obj_net = (struct network_info *)obj;

	return elem_net->family == obj_net->family &&
			elem_net->bytelen == obj_net->bytelen &&
			elem_net->bitlen == obj_net->bitlen &&
			!memcmp(elem_net->net, obj_net->net, elem_net->bytelen);
}

__u8 is_link_local(__u8 addr[16]){

	return addr[0] == 0xfe &&
			(addr[1] & 0xc0) == 0x80;
}

__s8 get_address_mask(char *mask, __u8 max, __u8 *out){

	char *end;
	unsigned long mask_dec;

	mask_dec = strtoul(mask, &end, 10);

	if(end == mask || end == NULL || mask_dec > max){
		return -1;
	}

	*out = mask_dec;

	return 0;

}

__s8 add_network(struct network_list *list, char *network){

	struct network_info temp;

	char copy[64];
	strncpy(copy, network, 64);
    char *mask = strchr(copy, '/');

	if(mask){
		*mask++ = '\0';
	}

	if(strchr(network, '.')){ //ipv4

		temp.family = AF_INET;
		temp.bytelen = 4;

	}else if(strchr(network, ':')){ //ipv6

		temp.family = AF_INET6;
		temp.bytelen = 16;

	}else{
		return -2; //neither ipv4 nor ipv6
	}

	if(!inet_pton(temp.family, copy, temp.net)){
		return -1;
	}

	__u8 max_mask = temp.bytelen* 8;

	if(mask){
		if(get_address_mask(mask, max_mask, &temp.bitlen)){
			return -1;
		}
	}else{
		temp.bitlen = max_mask;
	}

	//find a better way
	if(temp.family == AF_INET){
		add_element(&list->ipv4, &temp);
	}else{
		add_element(&list->ipv6, &temp);
	}

	return 0;
}

__u8 is_ipv6(void *obj){

	struct network_info *info = (struct network_info *)obj;

	return info->family == AF_INET6;
}

__u8 is_ipv4(void *obj){

	struct network_info *info = (struct network_info *)obj;

	return info->family == AF_INET;
}

__s16 parse_proto(char *proto){
	__s16 ret = -1;

	if(!strcmp(proto, "ipv4")){
		ret = AF_INET;
	}else if(!strcmp(proto, "ipv6")){
		ret = AF_INET6;
	}

	return ret;

}

void print_network(struct network_info *net, char* add_str){

	__u8 k;
	fprintf(stdout, "%sFamily: %s\n", add_str, net->family == AF_INET ? "IPV4" : "IPV6");
	fprintf(stdout, "%sAddress: ", add_str);

	if(net->family == AF_INET){
		for(k=0; k < 3; k++){
			fprintf(stdout, "%u.", net->net[k]);
		}
		fprintf(stdout, "%u\n", net->net[k]);
	}else{
		for(k=0; k < 14; k+=2){
			fprintf(stdout, "%02x%02x:", net->net[k], net->net[k+1]);
		}
		fprintf(stdout, "%02x%02x\n", net->net[k], net->net[k+1]);
	}

	fprintf(stdout, "%sBytelen: %u\n", add_str, net->bytelen);
	fprintf(stdout, "%sBitlen: %u\n", add_str, net->bitlen);
	fflush(stdout);

}
