#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/fib_rules.h>
#include <linux/if.h>
#include <linux/if_addr.h>
#include <linux/if_link.h>
#include <linux/genetlink.h>
#include <linux/mptcp.h>

#include <libnetlink.h>
#include <string.h>
#include <stdlib.h>

#include "req.h"

/*//obtains mptcp family using MPTCP_PM_NAME; (ps: stollen from iproute2)
	#include <linux/genetlink.h>

	struct rtnl_handle grth = { .fd = -1 };

    if(rtnl_open_byproto(&grth, 0, NETLINK_GENERIC) < 0){
        fprintf(stderr,"Failed to open generic connection!");
        _exit(-1);
    }

	//prints family
	fprintf(stdout, "$$$$$$$$$$$$$$$$$$$$$$  %d\n", genl_resolve_family(&grth,MPTCP_PM_NAME));
*/
#define MPTCP_FAMILY  29

struct request init_request(__u64 size){
	struct request req = {
		.req = (struct nlmsghdr *)calloc(size, sizeof(char)),
		.next_part = req.req,
		.total_size = 0,
		.max_size = size
	};

	return req;
}

void update_req_part(struct request *req){
	req->total_size += req->next_part->nlmsg_len;
	req->next_part = (struct nlmsghdr *)(((char *) req->next_part) + 
						NLMSG_ALIGN(req->next_part->nlmsg_len));
}

void update_req_size(struct request *req, __u64 next_part){

	if((req->max_size - req->total_size) < next_part){
		__u32 diff = req->next_part - req->req;
		req->max_size *= 2;
		req->req = (struct nlmsghdr *)realloc(req->req, req->max_size);
		req->next_part = req->req + diff;
	}
}

void finalize_multipart(struct request *req){
	struct nlmsghdr *part = req->next_part;

	memset(part, 0, sizeof(struct nlmsghdr));

	part->nlmsg_type = NLMSG_DONE;
	part->nlmsg_flags = NLM_F_REQUEST | NLM_F_MULTI;
	part->nlmsg_len = NLMSG_LENGTH(0);

	req->total_size += sizeof(struct nlmsghdr);
}

void add_rule(void *pointer, struct  mptcp_endpoint *address, __u16 extra_flags){

    //request
    struct{
        struct nlmsghdr nh;
        struct fib_rule_hdr frh;
        char buf[28];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    //NETLINK properties
    req->nh.nlmsg_flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->frh));
    req->nh.nlmsg_type = RTM_NEWRULE;

    //Rule properties
    req->frh.family = address->info.family;
    req->frh.action = FR_ACT_TO_TBL;
    req->frh.src_len = address->info.bytelen * 8; //ignoring mask

    //add address as attribute
    addattr_l(&req->nh, sizeof(*req), FRA_SRC, &address->info.net, address->info.bytelen);
    //add table id as attribute
    addattr32(&req->nh, sizeof(*req), FRA_TABLE, address->table_id);
}

void del_rule(void *pointer, struct mptcp_endpoint *address, __u16 extra_flags){

    //request
    struct{
        struct nlmsghdr nh;
        struct fib_rule_hdr frh;
        char buf[8];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    //NETLINK properties
    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->frh));
    req->nh.nlmsg_type = RTM_DELRULE;

    //Rule properties
    req->frh.family = address->info.family;
    req->frh.action = FR_ACT_TO_TBL;

    //add table id as attribute
    addattr32(&req->nh, sizeof(*req), FRA_TABLE, address->table_id);
}

//see if it can be done using multipart
void add_destiny(void *pointer, struct network_info *dest, __u32 table_id, __u32 index, __u16 extra_flags){

    //request
    struct{
        struct nlmsghdr nh;
        struct rtmsg rt;
        char buf[36];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    //NETLINK properties
    req->nh.nlmsg_flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->rt));
    req->nh.nlmsg_type = RTM_NEWROUTE;
    
    //Route properties
    req->rt.rtm_table = RT_TABLE_UNSPEC;
    req->rt.rtm_protocol = RTPROT_BOOT;
    req->rt.rtm_scope = RT_SCOPE_LINK;
    req->rt.rtm_type = RTN_UNICAST;
    req->rt.rtm_family = dest->family;
    req->rt.rtm_dst_len = dest->bitlen;

    //add table id as attribute
    addattr32(&req->nh, sizeof(*req), RTA_TABLE, table_id);

    //add dest 
    addattr_l(&req->nh, sizeof(*req), RTA_DST, &dest->net, dest->bytelen);

    //add interface index
    addattr32(&req->nh, sizeof(*req), RTA_OIF, index);
}

void del_destiny(void *pointer, struct network_info *dest, __u32 table_id, __u32 index, __u16 extra_flags){

    //request
    struct{
        struct nlmsghdr nh;
        struct rtmsg rt;
        char buf[36];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));
    
    //NETLINK properties
    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->rt));
    req->nh.nlmsg_type = RTM_DELROUTE;

    //Route properties
    req->rt.rtm_table = RT_TABLE_UNSPEC;
    req->rt.rtm_protocol = RTPROT_BOOT;
    req->rt.rtm_scope = RT_SCOPE_LINK;
    req->rt.rtm_type = RTN_UNICAST;
    req->rt.rtm_family = dest->family;
    req->rt.rtm_dst_len = dest->bitlen;

    //add table id as attribute
    addattr32(&req->nh, sizeof(*req), RTA_TABLE, table_id);

    //add dest 
    addattr_l(&req->nh, sizeof(*req), RTA_DST, &dest->net, dest->bytelen);

    //add interface index
    addattr32(&req->nh, sizeof(*req), RTA_OIF, index);
}

void add_gateway(void *pointer, struct network_info *gateway, __u32 table_id, __u32 index, __u16 extra_flags){

    //request
    struct{
        struct nlmsghdr nh;
        struct rtmsg rt;
        char buf[36];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));
    
    //NETLINK properties
    req->nh.nlmsg_flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->rt));
    req->nh.nlmsg_type = RTM_NEWROUTE;

    //Route properties
    req->rt.rtm_table = RT_TABLE_UNSPEC;
    req->rt.rtm_protocol = RTPROT_BOOT;
    req->rt.rtm_scope = RT_SCOPE_UNIVERSE;
    req->rt.rtm_type = RTN_UNICAST;
    req->rt.rtm_family = gateway->family;
	req->rt.rtm_dst_len = gateway->bitlen;

    //add table id as attribute
    addattr32(&req->nh, sizeof(*req), RTA_TABLE, table_id);
    
    //add gw 
    //verificar quando se tem que usar RTA_VIA em vez de RTA_GATEWAY
    addattr_l(&req->nh, sizeof(*req), RTA_GATEWAY, &gateway->net, gateway->bytelen);
    
    //add interface index
    addattr32(&req->nh, sizeof(*req), RTA_OIF, index);
}

void del_gateway(void *pointer, struct network_info *gateway, __u32 table_id, __u32 index, __u16 extra_flags){

    //request
    struct{
        struct nlmsghdr nh;
        struct rtmsg rt;
        char buf[36];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    //NETLINK properties
    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->rt));
    req->nh.nlmsg_type = RTM_DELROUTE;

    //Route properties
    req->rt.rtm_table = RT_TABLE_UNSPEC;
    req->rt.rtm_protocol = RTPROT_BOOT;
    req->rt.rtm_scope = RT_SCOPE_UNIVERSE;
    req->rt.rtm_type = RTN_UNICAST;
    req->rt.rtm_family = gateway->family;
	req->rt.rtm_dst_len = gateway->bitlen;

    //add table id as attribute
    addattr32(&req->nh, sizeof(*req), RTA_TABLE, table_id);
    
    //add gw 
    //verificar quando se tem que usar RTA_VIA em vez de RTA_GATEWAY
    addattr_l(&req->nh, sizeof(*req), RTA_GATEWAY, &gateway->net, gateway->bytelen);
    
    //add interface index
    addattr32(&req->nh, sizeof(*req), RTA_OIF, index);
}

void conf_mptcp_limits(void *pointer, __u32 subflows, __u32 addrs, __u16 extra_flags){

    struct {
        struct nlmsghdr nh;
        struct genlmsghdr gen;
        char buf[16];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->gen));
    req->nh.nlmsg_type = MPTCP_FAMILY;

    req->gen.cmd = MPTCP_PM_CMD_SET_LIMITS;
    req->gen.version = MPTCP_PM_VER;

    addattr32(&req->nh, sizeof(*req), MPTCP_PM_ATTR_SUBFLOWS, subflows);
    addattr32(&req->nh, sizeof(*req), MPTCP_PM_ATTR_RCV_ADD_ADDRS, addrs);
}

void conf_mptcp_limits_subflows(void *pointer, __u32 subflows, __u16 extra_flags){

    struct {
        struct nlmsghdr nh;
        struct genlmsghdr gen;
        char buf[8];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->gen));
    req->nh.nlmsg_type = MPTCP_FAMILY;

    req->gen.cmd = MPTCP_PM_CMD_SET_LIMITS;
    req->gen.version = MPTCP_PM_VER;

    addattr32(&req->nh, sizeof(*req), MPTCP_PM_ATTR_SUBFLOWS, subflows);
}

void conf_mptcp_limits_addrs(void *pointer, __u32 addrs, __u16 extra_flags){

    struct {
        struct nlmsghdr nh;
        struct genlmsghdr gen;
        char buf[8];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->gen));
    req->nh.nlmsg_type = MPTCP_FAMILY;

    req->gen.cmd = MPTCP_PM_CMD_SET_LIMITS;
    req->gen.version = MPTCP_PM_VER;

    addattr32(&req->nh, sizeof(*req), MPTCP_PM_ATTR_RCV_ADD_ADDRS, addrs);
}

void add_mptcp_endpoint(void *pointer, struct mptcp_endpoint *endpoint, __u32 index, __u16 extra_flags){

    struct {
        struct nlmsghdr nh;
        struct genlmsghdr gen;
        char buf[44];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->gen));
    req->nh.nlmsg_type = MPTCP_FAMILY;

    req->gen.cmd = MPTCP_PM_CMD_ADD_ADDR;
    req->gen.version = MPTCP_PM_VER;

    struct rtattr *attr_addr = addattr_nest(&req->nh, sizeof(*req),MPTCP_PM_ATTR_ADDR | NLA_F_NESTED);

    addattr8(&req->nh, sizeof(*req), MPTCP_PM_ADDR_ATTR_ID, endpoint->mptcp_id);

    __u32 flags = MPTCP_PM_ADDR_FLAG_SIGNAL | MPTCP_PM_ADDR_FLAG_SUBFLOW;
    addattr32(&req->nh, sizeof(*req), MPTCP_PM_ADDR_ATTR_FLAGS, flags);

    addattr32(&req->nh, sizeof(*req), MPTCP_PM_ADDR_ATTR_IF_IDX, index);

    addattr16(&req->nh, sizeof(*req), MPTCP_PM_ADDR_ATTR_FAMILY, endpoint->info.family);

	//is there another way?
    int type = endpoint->info.family == AF_INET ? MPTCP_PM_ADDR_ATTR_ADDR4 : MPTCP_PM_ADDR_ATTR_ADDR6;
    addattr_l(&req->nh, sizeof(*req), type, &endpoint->info.net, endpoint->info.bytelen);

    addattr_nest_end(&req->nh, attr_addr);
}

void del_mptcp_endpoint(void *pointer, struct mptcp_endpoint *endpoint, __u16 extra_flags){

    struct {
        struct nlmsghdr nh;
        struct genlmsghdr gen;
        char buf[12];
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->gen));
    req->nh.nlmsg_type = MPTCP_FAMILY;

    req->gen.cmd = MPTCP_PM_CMD_DEL_ADDR;
    req->gen.version = MPTCP_PM_VER;

    struct rtattr *attr_addr = addattr_nest(&req->nh, sizeof(*req),MPTCP_PM_ATTR_ADDR | NLA_F_NESTED);

    addattr8(&req->nh, sizeof(*req), MPTCP_PM_ADDR_ATTR_ID, endpoint->mptcp_id);

    addattr_nest_end(&req->nh, attr_addr);
}

void flush_mptcp_endpoints(void *pointer, __u16 extra_flags){

    struct {
        struct nlmsghdr nh;
        struct genlmsghdr gen;
    } *req = pointer;

    memset(req, 0 , sizeof(*req));

    req->nh.nlmsg_flags = NLM_F_REQUEST | extra_flags;
    req->nh.nlmsg_len = NLMSG_LENGTH(sizeof(req->gen));
    req->nh.nlmsg_type = MPTCP_FAMILY;

    req->gen.cmd = MPTCP_PM_CMD_FLUSH_ADDRS;
    req->gen.version = MPTCP_PM_VER;
}
