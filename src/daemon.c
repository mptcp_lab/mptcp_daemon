#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libnetlink.h>
#include <signal.h>
#include <stdlib.h>

#include "array.h"
#include "req.h"
#include "network.h"
#include "device.h"

#define BUFFSIZE 8192

/* TODO:
 * - fix table clean
 * - posix
 * - cleanup mptcp and other things
 * - check user permissions
 * - check errno?
 * - add acks?
 * - get tables ids to use the next available (maybe max table_id ++, or something like flags, use bits to choose, or have some kind of mask)
 * - handle errors (em vez de dar exit)
 * - handle signals
 * - LOGS
 * - configuration and arguments
 * - only dump links and do filtered dump of addr and route if possible 
 * - get maximum mptcp limits
 * - use pure netlink
 * - pretty prints [warnings, errors, info, etc...]
 */

/* ###ADD_RULE COMMENTS
 * increase table id, needs another way, something like how flags work
 * handle errors, like try again in some time and if it failed more than x times abort daemon
 * maybe handle errors async
 */

/* ###DEL_RULE COMMENTS
 * handle errors, like try again in some time and if it failed more than x times abort daemon
 * maybe handle errors async
 */

//// CONFIGURATION

struct network_list whitelist;
__u8 use_sec = 1;
__u8 use_prim = 1;

__u32 add_addr_accepted = 4;
__u32 subflows = 0;

__u8 protocol = AF_UNSPEC;

////

struct rtnl_handle rth = { .fd = -1 }; //para todas as comunicações excepto mptcp
struct rtnl_handle rth_mptcp = { .fd = -1 }; //para mptcp related communication

struct dev_hash interfaces;

void print_interfaces(){

	fprintf(stdout, "::::::::::::::::::::::::::::::::\n");
	for(__u8 i = 0; i < interfaces.size; i++){

		if(interfaces.tb[i]){
			struct dev_info *info = interfaces.tb[i];
			fprintf(stdout, "Interface %u\n", i);
			fprintf(stdout, "\tIndex: %u\n", info->index);
			fprintf(stdout, "\tAddresses:\n");
			for(__u8 j = 0; j < info->addr.count; j++){
				struct mptcp_endpoint *address = info->addr.array[j];
				fprintf(stdout, "\t\tNetwork Info:\n");

				print_network(&address->info, "\t\t\t");

				fprintf(stdout, "\t\tMPTCP id: %u\n", address->mptcp_id);
				fprintf(stdout, "\t\tTable id: %u\n", address->table_id);
				fprintf(stdout, "\t\tConfigured: %u\n", address->configured);
			}

			fprintf(stdout, "\tDestinies:\n");
			for(__u8 j = 0; j < info->dest.count; j++){
				print_network(info->dest.array[j], "\t\t");
				fprintf(stdout, "\n");
			}

			if(info->gw[0]){
				fprintf(stdout, "\tGateway IPV4:\n");
				print_network(info->gw[0], "\t\t");

			}

			if(info->gw[1]){
				fprintf(stdout, "\tGateway IPV6:\n");
				print_network(info->gw[1], "\t\t");
			}

			fprintf(stdout, "\tAddresses Configured: %u\n\n", info->addrs_conf);
		}

	}
	fprintf(stdout, "::::::::::::::::::::::::::::::::\n\n");
	fflush(stdout);
}

void print_whitelist(){
	fprintf(stdout, "################################\n");
	for(__u8 i = 0; i < whitelist.ipv4.count; i++){
		print_network(whitelist.ipv4.array[i], "");
		fprintf(stdout,"\n");
	}
	for(__u8 i = 0; i < whitelist.ipv6.count; i++){
		print_network(whitelist.ipv6.array[i], "");
		fprintf(stdout,"\n");
	}
	fprintf(stdout, "################################\n\n");
	fflush(stdout);
}


void conf_address(struct dev_info *info, __u8 addr, struct request *req_rt, struct request *req_mptcp){

	__u8 gw, (* func)(void *);

	struct mptcp_endpoint *address = info->addr.array[addr];

	if(address->info.family == AF_INET){
		gw = 0;
		func = is_ipv4;
	} else{
		gw = 1;
		func = is_ipv6;
	}

	__s16 index = any_element(&info->dest, func);
	if(index != -1 || info->gw[gw]){
		fprintf(stdout, "conf in address\n");
		fflush(stdout);
		update_req_size(req_rt, ADD_RULE_SIZE);
		add_rule(req_rt->next_part, address, NLM_F_MULTI);
		update_req_part(req_rt);

		if(index != -1){
			for(; index < info->dest.count; index++){
				struct network_info *destination = (struct network_info *)info->dest.array[index];
				if(destination->family == address->info.family){
					update_req_size(req_rt, ADD_ROUTE_SIZE);
					add_destiny(req_rt->next_part, destination, address->table_id, info->index, NLM_F_MULTI);
					update_req_part(req_rt);
				}
			}
		}

		if(info->gw[gw]){
			update_req_size(req_rt, ADD_ROUTE_SIZE);
			add_gateway(req_rt->next_part, info->gw[gw], address->table_id, info->index, NLM_F_MULTI);
			update_req_part(req_rt);
		}

		update_req_size(req_mptcp, ADD_ENDP_SIZE);
		add_mptcp_endpoint(req_mptcp->next_part, address, info->index, NLM_F_MULTI);
		update_req_part(req_mptcp);

		address->configured = 1;
		info->addrs_conf++;
	}
}

void unconf_address(struct dev_info *info, __u8 addr, struct request *req_rt, struct request *req_mptcp){

	__u8 gw;

	struct mptcp_endpoint *address = info->addr.array[addr];

	if(address->configured){
		update_req_size(req_rt, DEL_ROUTE_SIZE);
		del_rule(req_rt->next_part, address, NLM_F_MULTI);
		update_req_part(req_rt);

		for(__u8 index = 0; index < info->dest.count; index++){
			struct network_info *destination = (struct network_info *)info->dest.array[index];
			if(destination->family == address->info.family){
				update_req_size(req_rt, DEL_ROUTE_SIZE);
				del_destiny(req_rt->next_part, destination, address->table_id, info->index, NLM_F_MULTI);
				update_req_part(req_rt);
			}
		}

		gw = address->info.family == AF_INET ? 0 : 1;
		if(info->gw[gw]){
			update_req_size(req_rt, ADD_ROUTE_SIZE);
			del_gateway(req_rt->next_part, info->gw[gw], address->table_id, info->index, NLM_F_MULTI);
			update_req_part(req_rt);
		}

		update_req_size(req_mptcp, ADD_ENDP_SIZE);
		del_mptcp_endpoint(req_mptcp->next_part, address, NLM_F_MULTI);
		update_req_part(req_mptcp);

		address->configured = 0;
		info->addrs_conf--;
	}
}

typedef void (* route_function)(void *, struct network_info *, __u32, __u32, __u16);

void conf_route(struct dev_info *info, struct network_info *route, route_function func, struct request *req_rt, struct request *req_mptcp){

	__u8 pre_configured = info->addrs_conf;

	for(__u8 i = 0; i < info->addr.count; i++){
		struct mptcp_endpoint *address = (struct mptcp_endpoint *)info->addr.array[i];
		if(address->info.family == route->family){
			if(!address->configured){
				fprintf(stdout, "conf in route\n");
				fflush(stdout);
				update_req_size(req_rt, ADD_RULE_SIZE);
				add_rule(req_rt->next_part, address, NLM_F_MULTI);
				update_req_part(req_rt);

				update_req_size(req_mptcp, ADD_ENDP_SIZE);
				add_mptcp_endpoint(req_mptcp->next_part, address, info->index, NLM_F_MULTI);
				update_req_part(req_mptcp);

				address->configured = 1;
				info->addrs_conf++;
			}

			update_req_size(req_rt, ADD_ROUTE_SIZE);
			func(req_rt->next_part, route, address->table_id, info->index, NLM_F_MULTI);
			update_req_part(req_rt);
		}
	}

	if(!pre_configured && info->addrs_conf){
		subflows += 2;
		update_req_size(req_mptcp, CONF_SUB_SIZE);
		conf_mptcp_limits_subflows(req_mptcp->next_part, subflows, NLM_F_MULTI);
		update_req_part(req_mptcp);
	}

	finalize_multipart(req_rt);
	finalize_multipart(req_mptcp);
}

void unconf_route(struct dev_info *info, struct network_info *route, route_function func, struct request *req_rt, struct request *req_mptcp){

	__u8 pre_configured = info->addrs_conf;

	for(__u8 i = 0; i < info->addr.count; i++){
		struct mptcp_endpoint *address = (struct mptcp_endpoint *)info->addr.array[i];
		if(address->info.family == route->family){
			if(address->configured){
				update_req_size(req_rt, DEL_ROUTE_SIZE);
				func(req_rt->next_part, route, address->table_id, info->index, NLM_F_MULTI);
				update_req_part(req_rt);

				__u8 gw, (* comp)(void *);
				if(address->info.family == AF_INET){
					gw = 0;
					comp = is_ipv4;
				}else{
					gw = 1;
					comp = is_ipv6;
				}

				if(any_element(&info->dest, comp) == -1 && !info->gw[gw]){
					update_req_size(req_rt, DEL_RULE_SIZE);
					del_rule(req_rt->next_part, address, NLM_F_MULTI);
					update_req_part(req_rt);

					update_req_size(req_mptcp, DEL_ENDP_SIZE);
					del_mptcp_endpoint(req_mptcp->next_part, address, NLM_F_MULTI);
					update_req_part(req_mptcp);

					address->configured = 0;
					info->addrs_conf--;
				}

			}
		}
	}

	if(pre_configured && !info->addrs_conf){
		subflows -= 2;
		update_req_size(req_mptcp, CONF_SUB_SIZE);
		conf_mptcp_limits_subflows(req_mptcp->next_part, subflows, NLM_F_MULTI);
		update_req_part(req_mptcp);
	}

	finalize_multipart(req_rt);
	finalize_multipart(req_mptcp);

}

static void clean_handler(int signum){

	struct request req_rt = init_request(BUFFSIZE);
	struct request req_mptcp = init_request(BUFFSIZE);

    for(__u32 i = 0; i < interfaces.size; i++){
        if(interfaces.tb[i]){
			struct dev_info *info = interfaces.tb[i];
			for(__u8 j = 0; j < info->addr.count; j++){
				unconf_address(info, j, &req_rt, &req_mptcp);
			}

			remove_key(&interfaces, i);
		}
	}

	update_req_size(&req_mptcp, FLU_ENDP_SIZE);
	flush_mptcp_endpoints(req_mptcp.next_part, NLM_F_MULTI);
	update_req_part(&req_mptcp);

	update_req_size(&req_mptcp, CONF_LIMS_SIZE);
    conf_mptcp_limits(req_mptcp.next_part, 0, 0, NLM_F_MULTI);
	update_req_part(&req_mptcp);

	finalize_multipart(&req_rt);
	finalize_multipart(&req_mptcp);

	free(interfaces.tb);
	
	struct iovec iov;

	if(req_rt.total_size){

		iov.iov_base = req_rt.req;
		iov.iov_len = req_rt.total_size;

		if(rtnl_talk_iov(&rth, &iov, 1, NULL) < 0){
        	fprintf(stderr, "Failed to configure routing on listen\n");
        	_exit(-1);
		}

	}

	if(req_mptcp.total_size){
		iov.iov_base = req_mptcp.req;
		iov.iov_len = req_mptcp.total_size;

		if(rtnl_talk_iov(&rth_mptcp, &iov, 1, NULL) < 0){
        	fprintf(stderr, "Failed to configure mptcp on listen\n");
        	_exit(-1);
		}

	}

	free_array(&whitelist.ipv4);
	free_array(&whitelist.ipv6);

	_exit(0);

}

//use ctrl (check what it is)
int handle_rtnl(struct rtnl_ctrl_data *ctrl, struct nlmsghdr *n, void *arg ){

    int res = 0;
    struct dev_info *info;
	struct dev_reply reply;

	//reduce size
	struct request req_rt = init_request(BUFFSIZE);
	struct request req_mptcp = init_request(BUFFSIZE);

    switch(n->nlmsg_type){
        case RTM_NEWADDR:
            fprintf(stdout, "NEW ADDRESS\n");
			__u16 index;
			res = add_addr(n, &interfaces, &index);
            if(res < 0){
				clean_handler(0);
				fprintf(stderr, "There was an error\n");
				_exit(-1);
            }else if(res == 1){
				info = interfaces.tb[index];
				__u8 pre_configured = info->addrs_conf;
				conf_address(info, info->addr.count - 1, &req_rt, &req_mptcp);
				if(!pre_configured && info->addrs_conf){
					subflows += 2;
					update_req_size(&req_mptcp, CONF_SUB_SIZE);
					conf_mptcp_limits_subflows(req_mptcp.next_part, subflows, NLM_F_MULTI);
					update_req_part(&req_mptcp);
				}
				finalize_multipart(&req_rt);
				finalize_multipart(&req_mptcp);
            }else if(res == 2){

				info = interfaces.tb[index];

				for(__u8 index_addr = 0; index_addr < info->addr.count; index_addr++){
					unconf_address(info, index_addr, &req_rt, &req_mptcp);
				}

				subflows -= 2;
				update_req_size(&req_mptcp, CONF_SUB_SIZE);
				conf_mptcp_limits_subflows(req_mptcp.next_part, subflows, NLM_F_MULTI);
				update_req_part(&req_mptcp);

				finalize_multipart(&req_rt);
				finalize_multipart(&req_mptcp);

				remove_key(&interfaces, index);
			}
            break;
        case RTM_DELADDR:
            fprintf(stdout, "DELETE ADDRESS\n");
            res = rm_addr(n, &interfaces, &reply);
            if(res < 0){
				clean_handler(0);
                fprintf(stderr, "There was an error\n");
                _exit(-1);
            }else if(res){
                info = interfaces.tb[reply.index];
				__u8 pre_configured = info->addrs_conf;
				unconf_address(info, reply.data, &req_rt, &req_mptcp);
				if(pre_configured && !info->addrs_conf){
					subflows -= 2;
					update_req_size(&req_mptcp, CONF_SUB_SIZE);
					conf_mptcp_limits_subflows(req_rt.next_part, subflows, NLM_F_MULTI);
					update_req_part(&req_mptcp);

				}
				finalize_multipart(&req_rt);
				finalize_multipart(&req_mptcp);
				remove_by_index(&info->addr, reply.data);
            }
            break;
        case RTM_NEWROUTE:
            fprintf(stdout, "NEW ROUTE\n");
            res = add_route(n, &interfaces, &reply);
            if(res < 0){
				clean_handler(0);
                fprintf(stderr, "There was an error\n");
                _exit(-1);
            }else if(res){
                info = interfaces.tb[reply.index];
			 	if(res == 1){
					conf_route(info, info->dest.array[info->dest.count - 1], add_destiny, &req_rt, &req_mptcp);
				} else if(res == 2){
					conf_route(info, info->gw[reply.data], add_gateway, &req_rt, &req_mptcp);
				}
			}
            break;
        case RTM_DELROUTE:
            fprintf(stdout, "DELETE ROUTE\n");
            res = rm_route(n, &interfaces, &reply);
            if(res < 0){
				clean_handler(0);
                fprintf(stderr, "There was an error\n");
                _exit(-1);
            }else if(res){
				info = interfaces.tb[reply.index];
				if(res == 1){
					struct network_info destination = *(struct network_info *)info->dest.array[reply.data];
					remove_by_index(&info->dest, reply.data);
					unconf_route(info, &destination, del_destiny, &req_rt, &req_mptcp);
				}else if(res == 2){
					struct network_info gateway = *info->gw[reply.data];
					free(info->gw[reply.data]);
					info->gw[reply.data] = NULL;
					unconf_route(info, &gateway, del_gateway, &req_rt, &req_mptcp);
				}
			}
            break;
        case RTM_NEWLINK:
            fprintf(stdout, "NEW LINK\n");
            res = add_dev(n, &interfaces);
            if(res < 0){
				clean_handler(0);
                fprintf(stderr, "There was an error\n");
                _exit(-1);
            }else if(res){
				info = interfaces.tb[res];
				__u8 pre_configured = info->addrs_conf;
				for(__u8 index = 0; index < info->addr.count; index ++){
					unconf_address(info, index, &req_rt, &req_mptcp);
				}

				if(pre_configured && !info->addrs_conf){
					subflows -= 2;
					update_req_size(&req_mptcp, CONF_SUB_SIZE);
    				conf_mptcp_limits_subflows(req_mptcp.next_part, subflows, NLM_F_MULTI);
					update_req_part(&req_mptcp);
				}

				finalize_multipart(&req_rt);
				finalize_multipart(&req_mptcp);

				remove_key(&interfaces, res);
            }
            break;
        case RTM_DELLINK:
            fprintf(stdout, "DELETE LINK\n");
            res = rm_dev(n, &interfaces);

            if(res < 0){
				clean_handler(0);
                fprintf(stderr, "There was an error\n");
                _exit(-1);
            } else if(res){
				info = interfaces.tb[res];
				__u8 pre_configured = info->addrs_conf;
				for(__u8 index = 0; index < info->addr.count; index ++){
					unconf_address(info, index, &req_rt, &req_mptcp);
				}

				if(pre_configured && !info->addrs_conf){
					subflows -= 2;
					update_req_size(&req_mptcp, CONF_SUB_SIZE);
    				conf_mptcp_limits_subflows(req_mptcp.next_part, subflows, NLM_F_MULTI);
					update_req_part(&req_mptcp);
				}

				finalize_multipart(&req_rt);
				finalize_multipart(&req_mptcp);

				remove_key(&interfaces, res);
			}
            break;
    }

	print_interfaces();

	fflush(stdout);

	struct iovec iov;

	if(req_rt.total_size){

		iov.iov_base = req_rt.req;
		iov.iov_len = req_rt.total_size;

		if(rtnl_talk_iov(&rth, &iov, 1, NULL) < 0){
			clean_handler(0);
        	fprintf(stderr, "Failed to configure routing on listen\n");
        	_exit(-1);
		}

	}

	if(req_mptcp.total_size){
		iov.iov_base = req_mptcp.req;
		iov.iov_len = req_mptcp.total_size;

		if(rtnl_talk_iov(&rth_mptcp, &iov, 1, NULL) < 0){
			clean_handler(0);
        	fprintf(stderr, "Failed to configure mptcp on listen\n");
        	_exit(-1);
		}

	}

    return 0;
}


int main(int argc, char **argv){

	//add receive initial state from file (only for testing purposes) (-f, --file)
	//add namescape set up (for sockets if possible) (only for testing purposes) (-n, --namespace)
	//add blacklist (-b, --blacklist) (blacklist xor whitelist, unless there is a reason to allow both)
	//maybe add write requests to file
	//maybe add write acks to file
	
	if(signal(SIGINT, clean_handler) == SIG_ERR){
		fprintf(stderr, "There was an error setting up clean exit\n");
		_exit(-1);
	}
	
	whitelist.ipv4 = init_array(16, sizeof(struct network_info));
	whitelist.ipv6 = init_array(16, sizeof(struct network_info));
	
    while(*(++argv)){
        if(!strcmp(*argv, "--whitelist") || !strcmp(*argv, "-w")){

            char *net = strtok(*(++argv), ",");
            while(net){
                if(add_network(&whitelist, net) < 0){
					fprintf(stderr, "Invalid address\n");
				}
                net = strtok(NULL, ",");
            }

        }else if(!strcmp(*argv, "--secondary") || !strcmp(*argv, "-s")){
			use_sec = 1;
			use_prim = 0;
        }else if(!strcmp(*argv, "--primary") || !strcmp(*argv, "-p")){
			use_prim = 1;
			use_sec = 0;
		}else if(!strcmp(*argv, "--protocol") || !strcmp(*argv, "-P")){
			__s16 parsed = parse_proto(*(++argv));
			if(parsed < 0){
            	fprintf(stderr,"Invalid protocol!");
            	_exit(-1);
			}
			protocol = parsed;
		}else {
            fprintf(stderr,"Invalid option!");
            _exit(-1);
        }
    }

	fflush(stderr);

	print_whitelist();

    interfaces = init_dev_hash(16);

    unsigned int groups = 0;

	//explore RTNLGRP_IPV4_MROUTE
    groups |= (1 << (RTNLGRP_IPV4_ROUTE - 1));
    groups |= (1 << (RTNLGRP_IPV6_ROUTE - 1));

    groups |= (1 << (RTNLGRP_IPV4_IFADDR - 1));
    groups |= (1 << (RTNLGRP_IPV6_IFADDR - 1));

    groups |= (1 << (RTNLGRP_LINK - 1));
    
    if(rtnl_open(&rth, groups) < 0){
        fprintf(stderr,"Failed to open rtnetlink connection!");
        _exit(-1);
    }

    if(rtnl_open_byproto(&rth_mptcp, 0, NETLINK_GENERIC) < 0){
        fprintf(stderr,"Failed to open generic connection!");
        _exit(-1);
    }

    //dump link
    if(rtnl_linkdump_req(&rth, protocol) < 0 ){
        fprintf(stderr,"Failed to dump links!");
        _exit(-1);
    }

    if(rtnl_dump_filter(&rth, add_dev, &interfaces) < 0){
		clean_handler(0); //simpler cleaner
        fprintf(stderr,"Failed to populate devices!");
        _exit(-1);
    }
	
    //dump addr
    if(rtnl_addrdump_req(&rth, protocol, NULL) < 0 ){
        fprintf(stderr,"Failed to dump address!");
        _exit(-1);
    }

    if(rtnl_dump_filter(&rth, add_addr_wrapper, &interfaces) < 0){
		clean_handler(0); //simpler cleaner
        fprintf(stderr,"Failed to populate addr and props!");
        _exit(-1);
    }
	
    //dump routing
    if(rtnl_routedump_req(&rth, protocol, NULL) < 0 ){
        fprintf(stderr,"Failed to dump routes!");
        _exit(-1);
    }

    if(rtnl_dump_filter(&rth, add_route_wrapper, &interfaces) < 0){
		clean_handler(0); //simpler cleaner
        fprintf(stderr,"Failed to populate gw and dest!");
        _exit(-1);
    }

	print_interfaces();

	//memory allocated (in bytes) (not a struct nlmsghdr array)
	struct request req_rt = init_request(BUFFSIZE);

	struct request req_mptcp = init_request(BUFFSIZE);

    for(__u32 i = 0; i < interfaces.size; i++){
		//dont check here but maybe while receiving the interfaces addresses
        if(interfaces.tb[i]){
            struct dev_info *info = interfaces.tb[i];
			for(__u8 j = 0; j < info->addr.count; j++){
				conf_address(info, j, &req_rt, &req_mptcp);
			}

			if(info->addrs_conf){
				subflows += 2;
			}
		}
	}

	update_req_size(&req_mptcp, CONF_LIMS_SIZE);
    conf_mptcp_limits(req_mptcp.next_part, subflows, add_addr_accepted, NLM_F_MULTI);
	update_req_part(&req_mptcp);

	finalize_multipart(&req_rt);
	finalize_multipart(&req_mptcp);
	
	struct iovec iov;

	if(req_rt.total_size){

		iov.iov_base = req_rt.req;
		iov.iov_len = req_rt.total_size;

		if(rtnl_talk_iov(&rth, &iov, 1, NULL) < 0){
			clean_handler(0); //simpler cleaner
        	fprintf(stderr, "Failed to configure routing on listen\n");
        	_exit(-1);
		}

	}

	if(req_mptcp.total_size){
		iov.iov_base = req_mptcp.req;
		iov.iov_len = req_mptcp.total_size;

		if(rtnl_talk_iov(&rth_mptcp, &iov, 1, NULL) < 0){
			clean_handler(0); //simpler cleaner
        	fprintf(stderr, "Failed to configure mptcp on listen\n");
        	_exit(-1);
		}

	}

    //some kind of "clean" exit?
    if(rtnl_listen(&rth, handle_rtnl, NULL) < 0){
		clean_handler(0);
        fprintf(stderr, "Failed to listen rtnetlink socket\n");
        _exit(-1);
    }

    return 0;
}
