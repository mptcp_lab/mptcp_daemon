#ifndef ___DAEMON_MPTPC_ARRAY_H___
#define ___DAEMON_MPTPC_ARRAY_H___

#include <asm/types.h>

struct dyn_array{
	void **array;
	__u8 elem_size;
	__u8 count;
	__u8 size;
};

struct dyn_array init_array(__u8 num_elem, __u8 elem_size);

void add_element(struct dyn_array *array, void *elem);

__s16 find_element(struct dyn_array *array, void *obj, __u8 (*func)(void *, void *));

void remove_by_index(struct dyn_array *array, __u8 index);

__s16 remove_by_element(struct dyn_array *array, void *obj, __u8 (*func)(void *, void *));

__s16 any_element(struct dyn_array *array, __u8 (*func)(void *));

void free_array(struct dyn_array *array);

#endif
