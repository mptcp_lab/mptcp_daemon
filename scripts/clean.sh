#!/bin/bash

sudo ip rule delete table 1
sudo ip route delete 192.168.1.0/24 table 1
sudo ip route delete default table 1
