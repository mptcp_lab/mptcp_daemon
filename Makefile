CC := gcc
#split flags
CFLAGS := -std=c99 -Wall -O2 -Wextra -Wsign-compare -march=native -mtune=native -flto #-pg
LDFLAGS := -lnetlink -lmnl

#INC_DIR := include
SRC_DIR := src
BUILD_DIR := build
TARGET := mptcp_daemon

#find another way, since SRCS && SRCS_TEST only diff on one file (test.c vs daemon.c)
SRCS := $(shell find $(SRC_DIR) -name *.c -and -not -name test.c)
OBJS := $(SRCS:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)

SRCS_TEST := $(shell find $(SRC_DIR) -name *.c -and -not -name daemon.c)
OBJS_TEST := $(SRCS_TEST:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)

#add make install

# Compiles
$(BUILD_DIR)/$(TARGET): mkdir_build $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@ $(LDFLAGS)

# Clean
clean:
	rm -rf $(BUILD_DIR)

test: mkdir_build $(OBJS_TEST)
	$(CC) $(CFLAGS) $(OBJS_TEST) -o $(BUILD_DIR)/$@ $(LDFLAGS)

# Objects
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

mkdir_build:
	@mkdir -p $(BUILD_DIR)

